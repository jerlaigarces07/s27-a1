let http = require("http");

http.createServer(function(request, response) {
    if(request.url == "/" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end('Welcome to the Booking System');
    }

    if(request.url == "/profile" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end('Welcome to your profile!');
    }

    if(request.url == "/courses" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end('Here\'s our courses available');
    }
    if(request.url == "/addcourse" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end('Add a course to our resources');
    }
    if(request.url == "/updatecourse" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end('Update a course to our resources');
    }

    if(request.url == "/archivecourses" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end('Archive courses to our resources');
    }

}).listen(4000);

console.log('Server is running at localhost:4000');